const REQUIRED_FIELD = "Поле обязательно для заполнения"
const EMPTY_STRING = ""
const INVALID_EMAIL_ERROR = "Email невалидный"
const INVALID_PASSWORD_ERROR = "Пароль должен содержать как минимум 8 символов"
const TEXT_ERROR ='color-text-error'
const BORDER_ERROR ='color-border-error'

let flagEmailValid;
let flagPasswordValid;
let flagCheckboxValid;
let isValid

const regularValidation =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const email = document.getElementById("email");
const password = document.getElementById("password");
const checkbox = document.getElementById("checkbox");
const button = document.getElementById("submitBtn");
const emailErrorHidden = document.getElementById("email-error");
const passwordErrorHidden = document.getElementById("password-error");
const checkboxErrorHidden = document.getElementById("checkbox-error");
const labelEmail = document.getElementById("label-email");
const starEmail = document.getElementById("star-email");
const labelPassword = document.getElementById("label-password");
const starPassword = document.getElementById("star-password");
const checkboxMark = document.getElementById("checkbox-mark");
const starCheckbox = document.getElementById("star-checkbox");

const emailField = [emailErrorHidden,
    labelEmail,
    email,
    starEmail,]

const passwordField = [passwordErrorHidden,
    labelPassword,
    password,
    starPassword,]

button.addEventListener("click", event => {
    event.preventDefault();
    onSubmit();
});

const isMailValid = email => {
    if (email === EMPTY_STRING) {
        return setErrorColor(emailField, REQUIRED_FIELD)
    }

    isValid = regularValidation.test(String(email).toLowerCase());
    if (!isValid) {
        return setErrorColor(emailField, INVALID_EMAIL_ERROR)
    }

    flagEmailValid = true;
}

const isPasswordValid = password => {
    if (password === EMPTY_STRING) {
        return setErrorColor(passwordField, REQUIRED_FIELD)
    }

    isValid = password.length >= 8;
    if (!isValid) {
        return setErrorColor(passwordField, INVALID_PASSWORD_ERROR)
    }

    flagPasswordValid = true;
}

const isCheckboxValid = checked => {
    if (!checked) return setRedCheckbox()
    flagCheckboxValid = true;
}

const setErrorColor = (field, text) => {
    field[0].textContent = text;
    field[0].hidden = false;
    field[1].classList.add(TEXT_ERROR);
    field[2].classList.add(BORDER_ERROR);
    field[3].classList.add(TEXT_ERROR);
}

const UnSetErrorColor = field => {
    field[0].hidden = true;
    field[1].classList.remove(TEXT_ERROR);
    field[2].classList.remove(BORDER_ERROR);
    field[3].classList.remove(TEXT_ERROR);
}

const setRedCheckbox = () => {
    checkboxErrorHidden.hidden = false;
    checkboxMark.classList.add(BORDER_ERROR);
    starCheckbox.classList.add(TEXT_ERROR);
}

const UnSetErrorCheckbox = () => {
    checkboxErrorHidden.hidden = true;
    checkboxMark.classList.remove(BORDER_ERROR);
    starCheckbox.classList.remove(TEXT_ERROR);
}

const clearInputData = () => {
    email.value = EMPTY_STRING;
    password.value = EMPTY_STRING;
    checkbox.checked = false;
}

const onSubmit = () => {
    flagEmailValid = false
    flagPasswordValid = false
    flagCheckboxValid = false

    UnSetErrorColor(emailField);
    UnSetErrorColor(passwordField);
    UnSetErrorCheckbox();

    isMailValid(email.value)
    isPasswordValid(password.value)
    isCheckboxValid(checkbox.checked)

    if (flagEmailValid && flagPasswordValid && flagCheckboxValid) {
        console.log({email:email.value, password:password.value})
        clearInputData()
    }
}
