const FIRST_NUMBER_MESSAGE = 'Please, insert first number:'
const OPERATOR_NUMBER_MESSAGE = 'Please, choose operator (available:+, -, /, *): '
const SECOND_NUMBER_MESSAGE = 'Please, insert second number:'
const EMPTY_NUMBER_VALUE_ERROR= 'Число не указано'
const INVALID_NUMBER_VALUE_ERROR= 'число введено некорректно'
const EMPTY_OPERATOR_VALUE_ERROR= 'Не введён знак'
const INVALID_OPERATOR_VALUE_ERROR= 'Программа не поддерживает такую операцию'
const DIVISION_BY_ZERO_ERROR= `На ноль делить нельзя!`
const FIRST_OPERAND_NUMBER ='Первое'
const SECOND_OPERAND_NUMBER ='Второе'
const EMPTY_STRING= ''
const INCREMENT = '+'
const DECREMENT = '-'
const DIVISION= '/'
const MULTIPLICATION = '*'

const simpleCalculator = () => {

    const firstVar = prompt(FIRST_NUMBER_MESSAGE);
    const operatorValue = prompt(OPERATOR_NUMBER_MESSAGE);
    const secondVar = prompt(SECOND_NUMBER_MESSAGE);

    const numberValidation = (inputValue, operandNumber) => {
        if (!inputValue) {
            return alert(`${operandNumber} ${EMPTY_NUMBER_VALUE_ERROR}`)
        }

        if (isNaN(inputValue)) {
            return alert(`${operandNumber} ${INVALID_NUMBER_VALUE_ERROR}`)
        }

        return +inputValue
    }

    const operatorValidation = (operator) => {
        switch (operator) {
            case EMPTY_STRING:
                return alert(`${EMPTY_OPERATOR_VALUE_ERROR}`);
            case INCREMENT :
            case  DECREMENT:
            case  DIVISION:
            case  MULTIPLICATION:
                return operator
            default:
                alert(`${INVALID_OPERATOR_VALUE_ERROR}`)
        }
    }

    const counter = (firstNumber, operator, secondNumber) => {
        switch (operator) {
            case INCREMENT :
                return firstNumber + secondNumber
            case DECREMENT :
                return firstNumber - secondNumber
            case DIVISION :
                return firstNumber / secondNumber
            case MULTIPLICATION :
                return firstNumber * secondNumber
        }
    }

    const firstOperand = numberValidation(firstVar,  `${FIRST_OPERAND_NUMBER}`)
    const operator = operatorValidation(operatorValue)
    const secondOperand = numberValidation(secondVar,  `${SECOND_OPERAND_NUMBER}`)

    if (secondOperand === 0 && operator === DIVISION) {
        alert(`${DIVISION_BY_ZERO_ERROR}`)
    }

    return console.log(counter(firstOperand, operator, secondOperand))
}
simpleCalculator()