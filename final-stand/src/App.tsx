import React, {FC, ReactElement} from 'react';

import { Route, Routes } from 'react-router-dom';

import './css/App.css';
import CurrencyDesk from './components/CurrencyDesk';
import CurrencyExchange from './components/CurrencyExchange';
import { CurrencyContainer } from './container/CurrencyContainer';
import { Path } from './enum/Path';
import {Nullable} from "./Nullable";


export const App: FC = (): Nullable<ReactElement> => {
    const { Home, Other, Desk } = Path;

    return (
        <div className="wrapper">
            <div className="container">
                <Routes>
                    <Route
                        path={Home}
                        element={<CurrencyContainer UniversalComponent={CurrencyExchange} />}
                    />
                    <Route
                        path={Desk}
                        element={<CurrencyContainer UniversalComponent={CurrencyDesk} />}
                    />
                    <Route
                        path={Other}
                        element={<h1 className="invalid-path">404 page not found</h1>}
                    />
                </Routes>
            </div>
        </div>
    );
};
