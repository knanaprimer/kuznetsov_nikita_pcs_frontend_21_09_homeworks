import React, { FC, KeyboardEvent, useState } from "react";

import { useNavigate } from "react-router-dom";

import { CurrencyWithKeyType } from "../redux/actions";
import { KeyBordCode } from "../enum/KeyBordCode";
import { Path } from "../enum/Path";
import {
    EMPTY_STRING,
    FIRST_ELEMENT, INITIAL_ZERO, RUBLES,
    SECOND_ELEMENT,
    SPACE,
    THIRD_ELEMENT
} from "../constants/constants";

const IN = 'in';
const INVALID_VALUES_ERROR_ALERT = 'Invalid values. Try to type this:"15 usd in rub".';

const CurrencyExchange: FC<CurrencyExchangePropsType> = ({valutes}) => {
    const { PressEnter } = KeyBordCode;
    const { Desk } = Path;
    const [value, setValue] = useState<string>(EMPTY_STRING);
    const [result, setResult] = useState<string>(EMPTY_STRING);
    const redirect = useNavigate();

    const strParser = (str: string): ParserReturnType => {
        const arrStr = str.split(SPACE).filter(el => el !== IN);
        return {
            sumToExchange: arrStr[FIRST_ELEMENT],
            originalValute: arrStr[SECOND_ELEMENT],
            resultValute: arrStr[THIRD_ELEMENT],
        };
    };

    const onClickResult = () => {
        const values = strParser(value);
        let abroadCurrencyRate = INITIAL_ZERO;

        if (valutes) {
            const catcher = () => {
                const rule0 = values.resultValute && values.originalValute && values.sumToExchange;
                const rule1 = Object.keys(valutes).some(v => v === values.resultValute?.toUpperCase());
                const rule2 = Object.keys(valutes).some(v => v === values.originalValute?.toUpperCase());
                const rule3 = values.resultValute === RUBLES;
                const rule4 = values.originalValute === RUBLES;
                const rule5 = values.resultValute !== values.originalValute;
                return rule0 && rule5 && (rule1 || rule3) && (rule2 || rule4);
            }

            if (!catcher()) {
                alert(INVALID_VALUES_ERROR_ALERT);
                setResult(EMPTY_STRING);
            } else {
                if (values.resultValute === RUBLES) {
                    abroadCurrencyRate = valutes[values.originalValute?.toUpperCase()].Value;
                } else {
                    abroadCurrencyRate = valutes[values.resultValute?.toUpperCase()].Value;
                }

                const calculatedValute = values.originalValute === RUBLES
                    ? (+Number(values.sumToExchange) / abroadCurrencyRate).toFixed(2)
                    : (+Number(values.sumToExchange) * abroadCurrencyRate).toFixed(2);
                setResult(calculatedValute)
            }
        }
    }

    const handleKeyDown = (e: KeyboardEvent<HTMLInputElement>): void => {
        if (e.code === PressEnter) {
            onClickResult();
        }
    };

    const redirectToDesk = (): void => {
        redirect(Desk);
    };

    return (
        <div className="currency">
            <h1 className="page-header">Currency exchange</h1>
            <div className="currency-names">
                <div className="fields">
                    <label>
                        <span>Hello! You can exchange your money here!</span>
                        <input
                            value={value}
                            onKeyDown={e => handleKeyDown(e)}
                            onChange={e => setValue(e.currentTarget.value)}
                            placeholder="100 usd in rub"
                        />
                        <button className="see-result button" onClick={onClickResult} type="submit">
                            Exchange
                        </button>
                        <h2 className="result">{result}</h2>
                    </label>
                </div>
            </div>
            <button className="redirect button" onClick={redirectToDesk} type="button">
                DESK
            </button>
        </div>
    );
};
export default CurrencyExchange;

//--------------------types-----------
type CurrencyExchangePropsType = {
    valutes: CurrencyWithKeyType | null;
};

type ParserReturnType = {
    sumToExchange: string;
    originalValute: string;
    resultValute: string;
};