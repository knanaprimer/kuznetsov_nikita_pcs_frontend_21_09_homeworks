import { CurrencyReducersTypes, CurrencyWithKeyType} from "./actions";
import {ACTIONS_TYPE} from "../enum/Action-type";

const { SET_CURRENCY } = ACTIONS_TYPE;
const initialState: InitialStateType = {
    valutes: null,
};

export const currencyReducer = (
    action: CurrencyReducersTypes,
    state = initialState,
): InitialStateType => {
    switch (action?.type) {
        case SET_CURRENCY: {
            return {
                ...state,
                valutes: action.payload.valutes,
            };
        }
        default:
            return state;
    }
};

//----------types-----------
type InitialStateType = {
    valutes: CurrencyWithKeyType | null
}
