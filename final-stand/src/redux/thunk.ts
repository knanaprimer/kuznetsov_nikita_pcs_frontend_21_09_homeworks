import {AxiosError} from "axios";
import {currencyAPI} from "../api/currency-api";
import {setAppErrorAC, setAppStatusAC, setCurrency} from "./actions";
import {AppThunkType} from "./state";
import {Status} from "../enum/Status";

const { LOADING, SUCCEEDED, FAILED} = Status

export const getCurrencyTC = (): AppThunkType => {
    return (dispatch) => {
        dispatch(setAppStatusAC(LOADING))
        currencyAPI.getCurrency()
            .then((res) => {
                dispatch(setCurrency({USD: res.data.Valute.USD, EURO: res.data.Valute.EUR}))
                dispatch(setCurrency(res.data.Valute))
                dispatch(setAppStatusAC(SUCCEEDED))
            })
            .catch((err: AxiosError) => {
                dispatch(setAppErrorAC(err.message))
                dispatch(setAppStatusAC(FAILED))
            })
    }
}

