const DOT = '.'
const LETTER_F = "F"
const BD_DESCRIPTION = 'Наша база содержит данные следующих пользователей:'
const URL_WITH_ALL_USERS = 'https://reqres.in/api/users?per_page=12'
const FIRST_LETTER = 0
const FIRST_USER = 0
const KEY_NAME = 0
const SECOND = 2
const THIRD =3
const FOURTH = 4
const FIFTH = 5

const savedUsers = []
let filteredUsers = []
let stringWithFullNames = ''

const logPointNumber =(number)=>{
    console.log('-----------');
    console.log(`Пункт №${number}:`)
    console.log('-----------');
}
const borderSlicer = someString => someString .slice(1, -1)
const getFullNames = (users) => {
    stringWithFullNames = users.reduce((acc, user) => {
        acc = acc + ` ${user.first_name} ${user.last_name},`
        return acc
    }, [])
    return borderSlicer(stringWithFullNames) + DOT
}

fetch(URL_WITH_ALL_USERS)
    .then(res => res.json())
    .then(data => data.data.forEach(item => savedUsers.push(item)))
    .then(() => {
        logPointNumber(SECOND)
        savedUsers.forEach(item => console.log(item.last_name))
    })
    .then(() => {
        logPointNumber(THIRD)
        filteredUsers = savedUsers.filter(item => item.last_name[FIRST_LETTER] === LETTER_F)
        console.log(filteredUsers)
    })
    .then(() => {
        logPointNumber(FOURTH)
        console.log(BD_DESCRIPTION, getFullNames(savedUsers));
    })
    .then(()=>{
        logPointNumber(FIFTH)
        Object.entries(savedUsers[FIRST_USER]).forEach(key =>console.log(key[KEY_NAME]))
    })




