import axios from "axios";

const EMPTY_STRING = ''
const BASE_URL = 'https://reqres.in/api/users?per_page=12';

const instance =axios.create({
    baseURL: BASE_URL
})

export const usersAPI = {
    getUsers() {
        return instance.get(EMPTY_STRING);
    },
    getUser() {
        return instance.get(EMPTY_STRING);
    },
}