import './style/App.css';
import {Outlet, Route, Routes} from "react-router-dom";
import {User} from "./components/user/User";
import {Header} from "./components/header/Header";
import {Footer} from "./components/footer/Footer";
import {Users} from "./components/users/Users";
import {Welcome} from "./components/welcome/Welcome";

function App() {
    return (
        <div className="App">
            <div className="App-wrapper">
                <Routes>

                    <Route path="/" element={<Layout/>}>
                        <Route index element={<Welcome/>}/>

                        <Route path="/users" element={<UsersLayout/>}>
                            <Route index element={<Users/>}/>
                            <Route path={":userID"} element={<User/>}/>
                        </Route>
                    </Route>

                    <Route path='*' element={<h1>404 page not found</h1>}/>
                </Routes>
            </div>
        </div>
    )
}

export default App;

const Layout = () => {
    return (
        <div>
            <Header/>
            <main className="Main-Layout">
                <Outlet/>
            </main>

            <Footer/>
        </div>
    )
}

const UsersLayout = () => {
    return (
        <div>
            <Outlet/>
        </div>
    )
}
