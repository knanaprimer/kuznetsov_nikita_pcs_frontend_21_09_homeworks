import st from "./User.module.css"
import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import {usersAPI} from "../../api/users-api";

export const User = () => {
    const {userID} = useParams()
    const [user, setUser] = useState()

    const idNumber = userID.slice(4)

    useEffect(() => {
        usersAPI.getUser().then(res => {
            setUser(res.data.data[idNumber-1])
        })
    }, [userID, idNumber])

    if (user) {
        const {id, last_name, first_name, avatar} = user
        return (
            <div className={st.file}>
                <h2>{first_name} {last_name}</h2>
                <img src={avatar} alt="sorry, its to ogly"/>
                <div className={st.danger}>
                    A very dangerous person! On account of 12 robberies
                    and {id} murders!
                </div>
            </div>
        )
    }
    return "Sorry, this friend is dead!"
}