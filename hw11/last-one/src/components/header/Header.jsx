import st from "./Header.module.css"
import {Link} from "react-router-dom";

export const Header = () => {
    return (
        <div className={st.header}>
            <Link to={"/"} className={st.link}><h2>HOY-HOY-HOY</h2></Link>
            <div className={st.links}>
                <Link to={"users"} className={st.link}>Users</Link>
                <Link to={"/"} className={st.link}>Home</Link>
            </div>
        </div>
    )
}