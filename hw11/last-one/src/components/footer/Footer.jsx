import st from './Footer.module.css'

export const Footer = () => {
    return (
        <div className={st.footer}>
            <a href="https://google.com" rel="nofollow noopener noreferrer"
               target="_blank"
               className="header_logo">
                If you don't like my perfect styles go find something better in
                Hoogle!
            </a>
            <p className="footer_rights"> © 2013-2021 Все права вооружены и защищены </p>
            <p className="footer_company-info">
                "Свидетельство о регистрации СМИ ЭЛ №Сhjkll88-79182 выдано не-федеральной
                службой по надзору в сфере связей,
                информационных технологий и массовых беспорядков (Роскомподзором) 32
                сентября 2222 года"
                <br/>
                "Сетевое издание My ID, учредитель АНО «Brinza TV», главный редактор Kuz
                N.A., 120+"
            </p>
        </div>
    )
}