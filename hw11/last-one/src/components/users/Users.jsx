import {useEffect, useState} from "react";
import {usersAPI} from "../../api/users-api";
import {Link} from "react-router-dom";
import st from "./Users.module.css"

export const Users = () => {
    const [users, setUsers] = useState()

    useEffect(() => {
        usersAPI.getUsers()
            .then(res => setUsers(res.data.data))
    }, [])


    if (users) {

        return (
            <div>
                <h2 style={{color: 'lime'}}>12 friends of ocean</h2>

                {users.map(user =>
                    <div key={user.id}>
                        <Link to={`user${user.id}`} className={st.link}>
                            {user.first_name} {user.last_name}
                        </Link>
                    </div>
                )}
            </div>
        )
    }
    return "Sorry, Ocean dont have friends anymore"
}